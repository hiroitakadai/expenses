module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: '家計簿',
    htmlAttrs: { lang: 'ja' },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: '#3B8070' },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
  },
  render: {
    csp: {
      hashAlgorithm: 'sha256',
      policies: {
        'default-src': ['\'self\''],
        'img-src': ['\'self\'', 'data:', '*.google-analytics.com'],
        'style-src': ['\'self\'', '\'unsafe-inline\''],
        'script-src': ['www.google-analytics.com', '\'unsafe-eval\''],
        'connect-src': ['\'self\'', process.env.API_DOMAIN || 'expenses-api.local', 'sentry.io'],
        'report-uri': ['https://57492ecc2dec415383c71f3e73a68d7e@sentry.io/1882342']
      }
    }
  },
  env: {
    API_URL: process.env.API_URL,
    API_DOMAIN: process.env.API_DOMAIN,
    ENVIRONMENT: process.env.ENVIRONMENT,
  },
  plugins: [
    '~/plugins/axios',
  ],
  modules: [
    'bootstrap-vue/nuxt',
    ['cookie-universal-nuxt', { parseJSON: false }],
    'nuxt-helmet',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    ['@nuxtjs/google-analytics', { id: 'UA-154895743-2' }],
    '@nuxtjs/sentry'
  ],
  helmet: {
    frameguard: { action: 'deny' },
    hsts: {
      maxAge: 15552000,
      includeSubDomains: false
    },
    noCache: true
  },
  sentry: { dsn: 'https://57492ecc2dec415383c71f3e73a68d7e@sentry.io/1882342' }
}

