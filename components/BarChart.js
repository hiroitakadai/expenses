import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['chartData', 'options', 'styles'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  }
}
