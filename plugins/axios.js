export default function ({ app, redirect }) {
  app.$axios.onRequest(function(config) {
    if (app.$cookies.get('token')) {
      config.headers.common['Authorization'] = `Bearer ${app.$cookies.get('token')}`
    }
  })
  
  app.$axios.onError(function(error) {
    if (!error.response) {
      return
    }
    const code = error.response.status
    if (code === 401) {
      app.$cookies.remove('token')
      redirect('/login')
    }
  
  })
  
}