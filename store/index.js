export const state = () => ({
  token: null
})
  
export const mutations = {
  store(state, token) {
    state.token = token
  }
}
